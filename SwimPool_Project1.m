%%Read the image from the location
location='/Users/reetas/Desktop/SwimPoolImages/Image1.png';

csvPos = '/Users/reetas/Desktop/SwimPoolImages/ImagePool1.csv';
csvNeg = '/Users/reetas/Desktop/SwimPoolImages/ImageNon1.csv';


img_uncrop0 = imread(location);

img0 = im2double(img_uncrop0);


img_L = graythresh(img0);
img_black = img0 > img_L;

R = img0(:,:,1);
G = img0(:,:,2);
B = img0(:,:,3);

maxGB = max(G,B);

C1_val = R ./ maxGB;
C1 = atan(C1_val);
C1_normalized = (2.0 / pi) * C1;

maxRB = max(R, B);
C2_val = G ./ maxRB;
C2 = atan(C2_val);
C2_normalized = (2.0 / pi) * C2;

maxRG = max(R, G);
C3_val = B ./ maxRG;
C3 = atan(C3_val);
C3_normalized  = (2.0 / pi) * C3;

L1 = graythresh(C1_normalized);
L3 = graythresh(C3_normalized);

C1_black = C1_normalized < L1 ;%* 0.75;
C3_black = C3_normalized > L3;

bw = im2bw(C1_normalized, L1);
cc = bwconncomp(not(bw),8);
cc

labeled = labelmatrix(cc);
RGB_label = label2rgb(labeled, @spring, 'c', 'shuffle');

poolProps = regionprops(cc,'basic');
poolArea = [poolProps.Area];
poolMatrixLabel = zeros(size(C1_black));
for i =1 : cc.NumObjects
    for position = cc.PixelIdxList{i}
        poolMatrixLabel(position) = i;
    end
end

[l,b] = size(C1_black);
maxArea = 0.0;
for i = 1 : l
    for j = 1: b
        if C1_black(i,j) == 1
            index = poolMatrixLabel(i,j);
            if index > 0
                thisPoolArea = poolArea(index);
                if thisPoolArea > maxArea
                    maxArea = thisPoolArea;
                end
            end
        end
    end
end

poolProb = zeros(size(C1_black));
[l,b] = size(C1_black);
for i = 1: l
    for j = 1: b
        if C1_black(i,j) == 1
            index = poolMatrixLabel(i,j);
            if index > 0
                poolProb(i,j) = poolArea(index) / maxArea;
            end
        end
    end
end

MPos = csvread(csvPos);
MNeg = csvread(csvNeg);
MImg = vertcat(MPos, MNeg);

[L,B] = size(MImg);

target = zeros(L,1);
output = zeros(L,1);

for i=1:L
    y = MImg(i,1);
    x = MImg(i,2);
    t = MImg(i,3);
    o = poolProb(x,y);
    target(i,1) = t;
    output(i,1) = o;
end

[X, Y] = perfcurve(target, output, 1);
plot(X,Y)
title('ROC curve')
xlabel('False Positive Rate')
ylabel('True Positive Rate')



